import random
import json
from sys import stdout
from time import sleep

data = {
    "{HAMBATAN1}":"1.0",
    "{HAMBATAN2}":"2.0",
    "{HAMBATAN3}":"3.0",
    "{HAMBATAN4}":"4.0",
    "{HAMBATAN5}":"5.0",
    "{HAMBATAN6}":"6.0",
    "{TEGANGAN1}":"12.0"
}

while(True):
    for k in data:
        n = random.random()
        data[k] = "{:.2f}".format(n)
    json_data =json.dumps(data, sort_keys=True)
    print(json_data)
    stdout.flush()
    sleep(1)
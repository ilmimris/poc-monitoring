const app = document.getElementById("app");

let socketData = new WebSocket("ws://127.0.0.1:10000");

window.clearCanvas = () => {
  if (document.getElementById("graphContainer").innerHTML !== "")
    document.getElementById("graphContainer").innerHTML = "";
};

window.sleep = async (ms) => await new Promise((r) => setTimeout(r, ms));

let isRetrievingData = false;

window.retrieveData = async (component) => {
  //   console.log(socketData.readyState);
  if (socketData.readyState === socketData.OPEN) isRetrievingData = true;
  let el;
  socketData.onmessage = async function (event) {
    mapObj = JSON.parse(event.data);
    // console.log({ mapObj });
    while (isRetrievingData) {
      Object.keys(mapObj).map((variable) => {
        el = document.getElementById(variable);
        el.innerText = mapObj[variable];
      });
      await sleep(2 * 1000); // retrieve every ms s
    }
  };
};

window.loadXML = function (container) {
  clearCanvas();

  var url = document.getElementById("url-xml").value;
  if (!mxClient.isBrowserSupported()) {
    // Displays an error message if the browser is not supported.
    mxUtils.error("Browser is not supported!", 200, false);
  } else {
    // Creates the graph inside the given container
    var graph = new mxGraph(container);
    graph.setHtmlLabels(true);

    // Enables rubberband selection
    new mxRubberband(graph);

    var req = mxUtils.load(url);
    // console.log({ req });

    var node = req.getDocumentElement();
    // console.log({ node });
    // console.log(node.nodeName);

    if (node != null && node.nodeName == "mxGraphModel") {
      //   console.log("already mxGraphModel");
      var dec = new mxCodec(node.ownerDocument);
      //   console.log({ dec });
    } else if (node != null && node.nodeName == "mxfile") {
      //   console.log("get mxGraphModel");
      document.getElementById("textarea").value = node.outerHTML;
      document.getElementById("btn-decode").click();

      var mxGraphModel = document.getElementById("textarea").value;
      var diagrams = mxGraphModel;
      //   console.log({ diagrams });

      if (diagrams.length > 0) {
        var doc = mxUtils.parseXml(diagrams);
        node = doc.documentElement;
        var dec = new mxCodec(doc);
        // console.log({ dec });
      }
    } else {
      throw Error("Unreconized format");
    }

    dec.decode(node, graph.getModel());

    const regex = /{\w*}/gi;
    while ((m = regex.exec(container.innerHTML)) !== null) {
      // This is necessary to avoid infinite loops with zero-width matches
      if (m.index === regex.lastIndex) {
        regex.lastIndex++;
      }

      // The result can be accessed through the `m`-variable.
      container.innerHTML = container.innerHTML.replace(
        m[0],
        "<span id='" + m[0] + "'></span>"
      );
    }
    // console.log({ graph });
  }
};

window.run = () =>
  (app.innerHTML = `
<div id="graphContainer"
style="position: fixed;overflow:hidden;width:100%;height:100%;background:url('editors/images/grid.gif');cursor:default;">
</div>
`);

// app.innerHTML = `<button type="button" class="btn btn-primary" onclick="main(document.getElementById('graphContainer'))">Load</button>`;
app.innerHTML = `
<div class="input-group mb-3">
<textarea rows="40" cols="120" style="display: none;" id="textarea" placeholder="Drop files here or enter text" spellcheck="false" autocomplete="off" autocorrect="off" autocapitalize="off"></textarea>
<input type="checkbox" id="encodeCheckbox" checked="checked" style="display: none;"/>
  <input type="checkbox" id="deflateCheckbox" checked="checked" style="display: none;"/>
  <input type="checkbox" id="base64Checkbox" checked="checked" style="display: none;"/>
  <input type="text" class="form-control" id="url-xml" placeholder="link XML model" aria-label="link XML model" aria-describedby="btn-load" value="diagrams/sample-eng.drawio">
  <div class="input-group-append">
    <button class="btn btn-outline-secondary" type="button" id="btn-load" onclick="loadXML(document.getElementById('graphContainer'))">Load</button>
    <button type="button" class="btn btn-outline-primary" onclick="retrieveData(document.getElementById('graphContainer'))">Retrieve Data</button>
    <button type="button" class="btn btn-outline-primary" onclick="isRetrievingData=false;">Pause</button>
    <button id="btn-decode" style="display: none;" onclick="decode(document.getElementById('textarea').value);return false;">Decode</button>
  </div>
</div>`;
// app.innerHTML += `<button type="button" class="btn btn-primary" onclick="uploadXML(document.getElementById('graphContainer'))">Load file</button>`;
app.innerHTML += `
<div id="graphContainer"
    style="position: fixed;width:100%;height:100%;background:url('editors/images/grid.gif');cursor:default;">
</div>
`;

# Sample Monitoring

> Dashboard monitor for diagram like facility diagram, electrical component diagram, etc. using websocket as protocol

![Demo](demo.gif)

## Table of Contents (Optional)

- [Installation](#installation)
- [Usage](#installation)
- [License](#license)

---

## Installation

- Install [websocketd](http://websocketd.com/#download) to localuser
```bash
wget -q -O websocketd.zip https://github.com/joewalnes/websocketd/releases/download/v0.3.0/websocketd-0.3.0-linux_amd64.zip && unzip -j websocketd.zip websocketd -d $HOME/.local/bin && rm websocketd.zip
```
- Install `http-server`
```bash
npm i -g http-server
```

### Clone

- Clone this repo to your local machine using `https://gitlab.com/ilmimris/poc-monitoring`


---

## Usage

run `python data generator` with [websocketd](http://websocketd.com/#download) on port `10000`
```bash
websocketd --port=10000 python3 generator.py
```

run `node http-server`
```bash
http-server --port=5000 .
```

open browser `http://localhost:5000`

---

## License

[![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)

- **[MIT license](http://opensource.org/licenses/mit-license.php)**
- Copyright 2020 © <a href="http://mris.dev" target="_blank">ilmimris</a>.